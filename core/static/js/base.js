$(document).ready(function() {
$('.datatable').dataTable({
    "language": {
        "lengthMenu": "Mostrando _MENU_ Registros",
        "zeroRecords": "No se encontraron registros",
        "info": "Pagina _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros",
        "infoFiltered": "(Se filtraron _MAX_ Registros en total)",
        "search":         "Buscar",
        "paginate": {
            "first":      "Inicio",
            "last":       "Fin",
            "next":       "Siguiente <span aria-hidden='true'>&rarr;</span>",
            "previous":   "<span aria-hidden='true'>&larr;</span> Anterior"
        },
    }
} );
});