from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from modules.User.models import SMUMedico
from django.contrib.auth.decorators import login_required


class SMLogin(TemplateView):
    template_name = 'login.html'
    """
    def get_context_data(self, **kwargs):
        context = super(SMLogin, self).get_context_data(**kwargs)
        data = {'Nombre': 'anderson'}
        context.update(data)
        return context
    """
    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous():
            return HttpResponseRedirect('/Control')
        return TemplateView.get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        next = request.GET.get('next', '')
        if next == '':
            next = '/Control'
        username = request.POST.get('inputUser', '')
        password = request.POST.get('inputPassword', '')
        if username != '' and password != '':
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    print 'Autenticado: ' + user.username
                    login(request, user)
                    return HttpResponseRedirect(next)
        return HttpResponseRedirect('/Login')


@login_required(login_url='login')
def smlogout(request):
    logout(request)
    return HttpResponseRedirect('/Login')
