from django import forms
from .models import SMCEstudiante


class SMCEstudianteForm(forms.ModelForm):
    class Meta:
        model = SMCEstudiante
        fields = ('first_name', 'last_name', 'email', 'cedula', 'facultad')
        widgets = {
            'first_name': forms.TextInput(attrs={'class': ''}),
            'last_name': forms.TextInput(attrs={'class': ''}),
            'email': forms.TextInput(attrs={'class': ''}),
            'cedula': forms.TextInput(attrs={'class': ''}),
            'facultad': forms.Select(attrs={'class': ''}),
        }
