from django.views.generic import TemplateView
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import ListView, CreateView, UpdateView
from .models import SMCEstudiante
from .forms import SMCEstudianteForm


class SMCMedicamentosList(TemplateView):
    template_name = 'medicamentos/lista.html'


class SMCEstudianteList(ListView):
    template_name = "estudiante/list.html"

    def get(self, request, *args, **kwargs):
        person = SMCEstudiante.objects.all()
        return render_to_response(self.template_name, locals(), context_instance=RequestContext(request))


class SMCEstudianteCreate(CreateView):
    form_class = SMCEstudianteForm
    template_name = 'estudiante/create.html'

    def post(self, request, *args, **kwargs):

        estudiante = SMCEstudiante(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            email=request.POST['email'],
            cedula=request.POST['cedula'],
            facultad_id=request.POST['facultad'],
        )
        estudiante.save()

        return HttpResponseRedirect('/Control/Estudiante')


class SMCEstudianteUpdate(UpdateView):
    form_class = SMCEstudianteForm
    template_name = 'estudiante/update.html'
    model = SMCEstudiante

    def post(self, request, *args, **kwargs):

        SMCEstudiante.objects.filter(pk=int(kwargs['pk'])).update(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            email=request.POST['email'],
            cedula=request.POST['cedula'],
            facultad_=request.POST['facultad'],
        )

        return HttpResponseRedirect('/Control/Estudiante')
