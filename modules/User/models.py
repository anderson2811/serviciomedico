from django.db import models
from django.contrib.auth.models import User, Group


class SMUMedico(models.Model):
    user = models.OneToOneField(User, unique=True)
    profile = models.ForeignKey(Group)
    cedula = models.CharField(max_length=10)

    def __unicode__(self):
        return '(%s - %s)' % (self.user.username, self.user.first_name)

    class Meta:
        db_table = 'smu_medicos'
